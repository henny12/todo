const Express = require('express')
const layout = require('express-ejs-layouts')
const session = require('cookie-session')
const flashMessage = require('express-flash')
const authRouter = require('./route/auth')
const homeRouter = require('./route/home')

const app = Express()

app.use(session({
    name: "session-percobaan",
    keys: ["key-percobaan"],
    path: '/'
}))
app.use(flashMessage())
app.use(Express.urlencoded({ extended: true }));
app.set('view engine', 'ejs')
app.use(layout)

app.use((req, res, next) => {
    res.locals.sudahMasuk = req.session?.data?.sudahMasuk
    next()
})

app.use('/auth', authRouter)
app.use('/home', homeRouter)

app.listen(3000, () => {
    console.log('app berjalan pada port 3000')
})