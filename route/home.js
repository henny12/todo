const route = require('express').Router()
const homeController = require('../controller/home')
const isAuthMiddleware = require('../middleware/isAuthenticated')

route.use(isAuthMiddleware.isAuthenticated)
route.get('/', homeController.renderPageHome)
route.post('/create', homeController.handleCreateTodo)
route.post('/:id/delete', homeController.handleDeleteTodo)
route.get('/:id/edit', homeController.renderPageEdit)
route.post('/:id/update', homeController.handleUpdateTodo)

module.exports = route

