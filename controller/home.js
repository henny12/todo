const {PrismaClient} = require("@prisma/client");
const prisma = new PrismaClient();

let renderPageHome = async (req, res) => {
    let todoData = await prisma.todo.findMany({
        where: {
            userId : req.session?.data?.id
        }
    })
    res.render('home', {data: todoData})
}

let renderPageEdit = async (req, res) => {
    let {id} = req.params
    let editData = await prisma.todo.findFirst({
        where : {
            id : parseInt(id)
        }
    })
    res.render('edit', {data: editData})
}

let handleCreateTodo = async (req, res) => {
    let {judul, tanggal} = req.body
    const submitToDatabase = await prisma.todo.create({
        data : {
            title: judul,
            createdAt: tanggal ? new Date(tanggal) : new Date(Date.now()),
            updatedAt: tanggal ? new Date(tanggal) : new Date(Date.now()),
            userId: req.session?.data?.id
        }
    })
    if (submitToDatabase) {
        res.redirect('/home')
    }
}

let handleDeleteTodo = async (req, res) => {
    try {
        let {id} = req.params
        const deleteFromDatabase = await prisma.todo.delete({
            where: {
                id: parseInt(id)
            }
        })
        if (deleteFromDatabase) {
            res.redirect('/home')
        }
    }
    catch (e) {
        console.log(e)
    }
}

let handleUpdateTodo = async (req, res) => {
    try {
        let {id} = req.params;
        let {judul} = req.body;
        const updateFromDatabase = await prisma.todo.update({
            where : {
                id: parseInt(id)
            },
            data: {
                title: judul
            }
        })
        if (updateFromDatabase) {
            res.redirect('/home')
        }
    }
    catch (e) {
        console.log(e)
    }
}

module.exports = {renderPageHome, renderPageEdit, handleCreateTodo, handleDeleteTodo, handleUpdateTodo}