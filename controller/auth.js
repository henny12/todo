const {PrismaClient} = require("@prisma/client");
const bcrypt = require("bcrypt");
const prisma = new PrismaClient();


let renderPageDaftar = (req, res) => {
    res.render('daftar')
}

let renderPageMasuk = (req, res) => {
    res.render('masuk')
}

let handleSignUp = async (req, res) => {
    try {
        let {uname, email, password} = req.body
        const checkIfUserRegistered = await prisma.user.findUnique({
            where: {
                email : email
            }
        })
        if (checkIfUserRegistered) {
            req.flash('error', 'anda sudah terdaftar!')
            res.redirect('/auth/daftar')
        }
        const insertUserToDatabase = await prisma.user.create({
            data : {
                username: uname,
                email: email,
                password: await bcrypt.hash(password, 10)
            }
        })
        if (insertUserToDatabase) {
            res.redirect('/auth/masuk')
        }
    }
    catch (e) {
        console.log(e)
    }
}

let handleSignIn = async (req, res) => {
    try {
        let {email, password} = req.body
        const checkIfUserRegistered = await prisma.user.findUnique(
            {
                where: {
                    email: email
                }
            }
        )
        if (!checkIfUserRegistered) {
            req.flash('error', 'anda belum terdaftar!')
            res.redirect('/auth/daftar')
        }
        const isPasswordValid = await bcrypt.compare(password, checkIfUserRegistered.password)
        if (!isPasswordValid) {
            req.flash('error', 'Password salah!')
            res.redirect('/auth/masuk')
        }
        req.session.data = {
            ...checkIfUserRegistered,
            sudahMasuk : true
        }
        res.redirect('/home')
    }
    catch (e) {
        console.log(e)
    }
}

let logout = async (req, res) => {
    try {
        req.session = null
        res.redirect('/auth/masuk')
    }
    catch (e) {
        console.log(e)
    }
}

module.exports = {renderPageDaftar, renderPageMasuk, handleSignUp, handleSignIn, logout}