const myPromise = (num) => {
    return new Promise((resolve, reject) =>{
        if (num) {
            setTimeout(() => {
                resolve('true!');
            }, 2000);
        } else {
            reject("false")
        }
    })
}

myPromise(1)
.then((data) => console.log(data))
.catch((data) => console.log(data))